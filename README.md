# Java - Administration d'un hôpital

Implémente l'héritage et modélise des classes en UML

## Ressources

- [CodeCademy - Learn Java](https://www.codecademy.com/learn/learn-java)

## Contexte du projet

Tu dois développer un système simplifié pour gérer les patients et le personnel médical d’un hôpital. Ce système devra permettre l’enregistrement des patients, le stockage des informations médicales, et la gestion des médecins et des infirmiers.

### Création du code du programme

Crée un nouveau projet Java et implémente et teste le programme suivant :

#### Hospital (point d'entrée du programme)

- Méthode :
  - `main(String[] args)` : elle te servira à créer des instances des autres classes, pour tester que tout fonctionne correctement.

#### Person (classe abstraite)

- Attributs : `name` (String), `age` (int), `socialSecurityNumber` (String)
- Méthodes :
  - `getName()`
  - `getAge()`
  - `getSocialSecurityNumber()`

#### Medication (classe)

- Attributs : `name` (String), `dosage` (String)
- Méthodes :
  - `getInfo()` : retourne les informations sur le médicament

#### Illness (classe)

- Attributs : `name` (String), `medicationList` (ArrayList<Medication>)
- Méthodes :
  - `addMedication(Medication medication)` : ajoute un médicament à la liste
  - `getInfo()` : retourne les informations sur la maladie et la liste des informations des médicaments

#### Patient (classe, hérite de Person)

- Attributs : `patientId` (String), `illnessList` (ArrayList<Illness>)
- Méthodes :
  - `addIllness(Illness illness)` : ajoute une maladie à la liste du patient
  - `getInfo()` : retourne les informations du patient (nom, âge, numéro de sécurité sociale et les informations sur ses maladies)

#### Care (interface)

- Méthodes :
  - `void careForPatient(Patient patient)` : pas d'implémentation par défaut
  - `void recordPatientVisit(String notes)` : implémentation par défaut qui affiche le contenu de la note

#### MedicalStaff (classe abstraite, hérite de Person, implémente Care)

- Attributs : `employeeId` (String)
- Méthodes abstraites :
  - `getRole()`

#### Doctor (classe, hérite de MedicalStaff)

- Attributs : `specialty` (String)
- Méthodes :
  - `getRole()` : affiche "Doctor"
  - `careForPatient(Patient patient)` : affiche "Doctor $doctorName cares for $patientName"

#### Nurse (classe, hérite de MedicalStaff)

- Méthodes :
  - `getRole()` : affiche "Nurse"
  - `careForPatient(Patient patient)` : affiche "Nurse $nurseName cares for $patientName"

### Diagrammes de classe

Une fois que tu as testé que ton implémentation fonctionne, ta seconde mission consiste à modéliser tes classes à l'aide d'un diagramme de classe UML.

Voici quelques vidéos pour apprendre la modélisation UML :

- [UML - Diagrammes de classes - 1. Classes et associations](https://www.youtube.com/watch?v=8VMMu-vcF60)
- [UML - Diagrammes de classes - 2. Associations particulières, héritage](https://www.youtube.com/watch?v=nRqTXoiNUHk)
- [UML - Diagrammes de classes - 3. Contraintes](https://www.youtube.com/watch?v=a3DWVNWg2Oo)
- [UML - Diagrammes de classes - 4. Opérations](https://www.youtube.com/watch?v=RxkarRkq10o)

Et si tu préfères le format écrit plutôt que des vidéos :

- [UML Cours 1 : Diagrammes de classes : associations](https://lipn.univ-paris13.fr/~gerard/uml-s2/uml-cours01.html)
- [UML Cours 2 : Diagrammes de classes : héritage](https://lipn.univ-paris13.fr/~gerard/uml-s2/uml-cours02.html)

## Modalités pédagogiques

En autonomie, sur trois jours

## Modalités d'évaluation

Il sera évalué que :

- les classes, interfaces, attributs et méthodes attendus sont bien créés
- la méthode `main` de la classe `Hospital` permet bien de tester toutes les classes et leurs méthodes
- le diagramme UML contient bien la modélisation attendue

## Livrables

- Un lien vers un dépôt GitLab
- Un lien vers le diagramme UML

## Critères de performance

- Utiliser les normes de codage du langage
- La documentation technique de l’environnement de travail est comprise
- Utiliser un outil de gestion de versions
