public abstract class Person {

    private String name;
    private int age;
    protected String socialSecurityNumber;
    private boolean connected;

    Person(String name, int age, String socialSecurityNumber) {
        this.name = name;
        this.age = age;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getName() {
        return this.name;
    }

    public int getAge() {
        return this.age;
    }

    public String getSocialSecurityNumber() {
        return this.socialSecurityNumber;
    }

    // cas particulier : les getter des booléens commence par "is" et pas par "get"
    public boolean isConnected() {
        return this.connected;
    }
}
