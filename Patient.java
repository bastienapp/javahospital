import java.util.ArrayList;

public class Patient extends Person {

    private String patientId;
    private ArrayList<Illness> illnessList = new ArrayList<>();

    Patient(String name, int age, String socialSecurityNumber, String patientId) {
        super(name, age, socialSecurityNumber);
        this.patientId = patientId;
    }

    public void addIllness(Illness illness) {
        this.illnessList.add(illness);
    }

    public String getInfo() {
        return this.getName() + " " + this.getAge() + " " + this.socialSecurityNumber + " " + this.patientId;
    }
}
