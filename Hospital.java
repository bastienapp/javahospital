import java.util.ArrayList;
import java.util.List;

class Hospital {

    public static void main(String[] args) {

        // Person michel = new Person("Michel", 50, "446464");

        Medication paracetamol = new Medication("Paracétamol", "1000");
        Medication advil = new Medication("Advil", "512");

        System.out.println(paracetamol.getInfo());

        Illness fridaySickness = new Illness("Friday sickness");
        System.out.println(fridaySickness.getInfo());

        /*
        ArrayList<Medication> lupusMedication = new ArrayList<>();
        lupusMedication.add(paracetamol);
        lupusMedication.add(advil);
         */

        Illness lupus = new Illness("Lupus",
            new ArrayList<>(List.of(paracetamol, advil)));
        System.out.println(lupus.getInfo());
    }
}